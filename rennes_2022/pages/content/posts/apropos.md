---
title: À propos de
slug: À propos de
description: null
draft: false
menu:
  main:
    name: À propos de
    weight: 111
url: /apropos/
---

L'idée d'un rencontre entre des ingénieurs travaillant dans des projets de
*santé numérique* à Inria part du constat que les ingénieurs de différents
centres ne se connaissent pas et que certains outils/procédures communs dans le
processus de développement logiciel au sein des équipes méritent d'être
partagés.

Une version initiale de ces échanges est disponible ici:

https://notes.inria.fr/hWTNx9QtQNOmS4pi3b5V1g#